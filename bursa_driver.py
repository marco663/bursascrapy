
import scrapy
from scrapy.crawler import CrawlerProcess

import sys
# sys.path.append('./BursaScrapy/spiders/')
from BursaScrapy.spiders.bursa_spider import BursaSpider

import pandas as pd 

# # Displaying the python version you are using for this program
# print("=====PYTHON VERSION=====")
# print(sys.version)
# print("========================\n")


# Initialize the prcess and the spider

settings = {
    'LOG_ENABLED': True,
    'LOG_LEVEL': 'DEBUG'
}

process = CrawlerProcess(settings = settings)


# # Call multiple spiders in parallel
# start_page = 1
# end_page = 20

# divider = 10
# # page_range = end_page - start_page + 1
# # page_each_spider_handlee = 10

# for i in range(10):
#     print("start", start_page + 10*i, end=" ")
#     print("end", 10*i + 10)
#     process.crawl(BursaSpider, start_page=start_page + 10*i, end_page=10*i + 10, is_header=True)

process.crawl(BursaSpider, start_page=1, end_page=5, is_header=True)

# checking how many pages are there in the bursa website
# print("\n=====CHECKING TOTAAL PAGES=====\n")
# process.start()

print("="*5, "CLEAR THE TEST FILE", "="*5, "\n")

# empty the file content
open('./test_data.csv', 'w').close()

print("="*5, "START SCRAPING DATA", "="*5, "\n")
process.start()

# print("="*5, "SORTING THE TABLE", "="*5, "\n")

df = pd.read_csv('./test_data.csv')
df = df.sort_values(by=['No.'])
df.to_csv('./test_data.csv', index=False)

print("="*5, "FINISHED", "="*5, "\n")
