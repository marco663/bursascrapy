# A Web Scraping Project using Python and Scrapy


## Description:  
Created a spider program that scrapes the information of the company announcements 
from all the webpages in Bursa Malaysia website https://www.bursamalaysia.com/market_information/announcements/company_announcement?page=1

## The information the program will get
1. Company Name
1. Title
1. Source URL
1. Description


## Usage
To start the scraping processing and sort the result csv by the 'No.' value, type:

`scrapy crawl bursa -o scraped_bursa_data.csv; python3 sort_data.py`


## Skills Overview
1. learned to select the html element using xpath selector
2. learned to use item as a container to store the scraped data 
3. learned to use item loader as an instruction of how to process the scraped data and fill them into the item object
4. learned to direct the spider to different pages in the website using `response.follow`
5. learned to pass-in the data (ex: itemloader) to the request through `request.meta`

## Things that can be improved
1. Unefficient Scraping Speed, think of calling multiple spiders and scrape the webpages in parallel