import sys
import scrapy
import re
import logging
from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose, TakeFirst
from BursaScrapy.items import BursaScrapyItem
import pandas as pd 

# Bursa Item loader which is instructed to populate the item 
# by particualr rules
class BursaLoader(ItemLoader):

    # set the default input processor to retrieve the 
    # stripped string content 
    default_input_processor = MapCompose(str.strip)

# Inherented Spider Class which defines
# how the website will be scraped
class BursaSpider(scrapy.Spider):
    name = "bursa"

    source_url_prefix = "https://disclosure.bursamalaysia.com/FileAccess/viewHtml?e="
    next_page_url_prefix = "https://www.bursamalaysia.com/market_information/announcements/company_announcement?page="

    # Function which defines how to call the requests 
    # the requests are started in order from start_page to end_page
    def start_requests(self):
        for i in range(self.start_page, self.end_page+1):
            link = BursaSpider.next_page_url_prefix + str(i)
            yield scrapy.Request(link)

    
    # Function which defines how the spider is initialized
    # It can be setted to scrape a particular range of pages by 
    # passing the start_page, end_page
    def __init__(self, start_page=1, end_page=1, is_header=False, *args, **kwargs):
        super(BursaSpider, self).__init__(*args, **kwargs)

        # convert the arguments into integer type
        self.start_page = int(start_page)
        self.end_page = int(end_page)
        self.is_header = is_header

    # Function defines how to scraped the structural data
    def parse(self, response):

        # create a lodaer
        loader = BursaLoader(item=BursaScrapyItem(), response=response)

        # get the number string
        loader.add_xpath('numbers', '//*[@id="table-announcements"]/tbody/tr/td[1]/text()')
        
        # get the list of companies names
        company_lst = response.css('#table-announcements .text-left:nth-child(3)').xpath('string(*//text())').getall()
        loader.add_value('company_names', company_lst)
        
        # get the list of announcement dates
        loader.add_xpath('announcement_dates', '//td[2]//div[2]/text()')

        # get titles 
        loader.add_css('titles', '.text-left+ .text-left a::text')

        # retrieve the id of the post by regex search
        # then add the full URl value to the loader
        id_list = loader.get_css('.text-left+ .text-left a::attr(href)', re='id=([0-9]+)') 
        loader.add_value('source_urls', id_list, 
                        MapCompose(lambda x: BursaSpider.source_url_prefix + x))
        
        # output the item
        yield loader.load_item()
        
    # Go through the list of links
    # and instruct the spider to extract the inf we want from each webpage 
    # return a list of descripts text
    def extract_descriptions_by_links(self, response, links):
        
        pass 

        # print("Entered edbl...")

        # descriptions = list()

        # link = links[0]
        
        # print("Sending Request to", link)

        # request = scrapy.Request(url=link, callback=self.extract_description_text_node)
        
        # yield request

    # Function that extracts the text node using selector
    # return the string of the text node
    def extract_description_text_node(self, response):

        pass

        # print("Entered edtn...")

        # print("Receive request from", response.url)

        # des = response.xpath("string(//td[contains(. , 'Description')]/../td[2])").extract_first()
        
        # print("The des is", des)

        # response.meta['des_str'] = des

            
        
    # from BursaScrapy.spiders.bursa_spider import BursaSpider, BursaLoader   


    ## ===== SELECTOR SYNTAX =====
    ## dates
    #       response.xpath('//td[2]//div[2]/text()').getall()
    ## company names
    #       response.css('#table-announcements .text-left:nth-child(3) a::text').getall()
    #                     
    # response.css('#table-announcements .text-left:nth-child(3)').xpath('string(*//text())').getall()

    ## titles
    #       response.css('.text-left+ .text-left a::text').getall()
    ## title_urls 
    #       response.css('.text-left+ .text-left a::attr(href)').getall()
    ## description content
    #       response.xpath("string(//td[contains(. , 'Description')]/../td[2])").extract_first()
    ## ===========================

    ## ===== REGEX SEARCH ========
    ## Announcement's id from title_url
    #        announcement_id = re.search("id=([0-9]+)", title_url).group(1)
    ## ===========================

    ## ===== FULL URL ============
    ## SOURCE URL
    #       source_url = "https://disclosure.bursamalaysia.com/FileAccess/viewHtml?e=" + announcement_id
    ## NEXT PAGE
    #       next_page = 'https://www.bursamalaysia.com/market_information/announcements/company_announcement?page=' + page_num + '/
    ## ===========================  

    ## ===== Note ============
    ## 1. Practice: 
    ##      Use scrapy shell "URL" to test the behavior of the program
    ##          a. selector -- response.add_css/add_xpath
    ##          b. loader
    ##              -- l.get_css/xpath  
    ##              -- l.add_css/xpath, and check the loaded item
    ##          c. spider -- parse function
    ## 2. Serializer 
    ## 3. CsvItemExporter
    ## ===========================


    ## * DETAIL SCRAPE INSTRUCTION
    # def scrape_detail(self, response):
    #     res_map = response.meta['res_map']
    #     des = response.xpath("string(//td[contains(. , 'Description')]/../td[2])").extract_first()
    #     res_map['Description'] = des.strip()
    #     yield res_map

    # # * THE PARSE FUNCTION CODE
    #     # CHECK THE LAST PAGE NUMBER
    #     total_pages = int(response.css('#total_page ::attr(data-val)').get())
    #     print("Total pages:", total_pages, "\n")
    #     print('scraping page', BursaSpider.page_number - 1, '...')

    #     # SCRAPED INFORMATION WE WANTED
    #     company_names = response.css('#table-announcements .text-left:nth-child(3) a::text').getall()
    #     titles = response.css('.text-left+ .text-left a::text').getall()
    #     title_urls = response.css('.text-left+ .text-left a::attr(href)').getall()

    #     # RETRIEVE THE ID OF THE ANNOUNCEMENT AND DIRECT TO THE DETAIL PAGE
    #     for i in range(0, len(company_names)):
    #         title_url = title_urls[i]
    #         announcement_id = re.search("id=([0-9]+)", title_url).group(1)
    #         detail_source_url = "https://disclosure.bursamalaysia.com/FileAccess/viewHtml?e=" + announcement_id
    #         res_map = {'No.': BursaSpider.item_number,
    #                 'Company Name': company_names[i].strip(),
    #                 'Title': titles[i].strip(),
    #                 'Source URL': detail_source_url}
    #         BursaSpider.item_number += 1
    #         request = scrapy.Request(detail_source_url, callback=self.scrape_detail)            
    #         request.meta['res_map'] = res_map
    #         yield request 

    #     # AFTER SRAPING THE DETAIL, DIRECT TO NEXT PAGE
    #     next_page = 'https://www.bursamalaysia.com/market_information/announcements/company_announcement?page=' + str(BursaSpider.page_number) + '/'
    #     # Up to 78410
    #     if BursaSpider.page_number <= 1:
    #             BursaSpider.page_number += 1
    #             yield response.follow(next_page, callback=self.parse)