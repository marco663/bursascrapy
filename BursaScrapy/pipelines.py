# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
from scrapy.exporters import CsvItemExporter
from BursaScrapy.items import BursaScrapyItem

class BursaScrapyPipeline(object):

    def process_item(self, item, spider):
        
        return item
        # print("Bursa Process Item is called!!")

        # # print(item)

        # export_fields = ['numbers', 'announcement_dates',
        #                  'company_names', 'titles', 
        #                  'source_urls']

        # with open('test_data.csv', 'wb') as csvfile:
        #     exporter = CsvItemExporter(file=csvfile,
        #                             include_headers_line=True,
        #                             fields_to_export=export_fields)

        #     exporter.start_exporting()

        #     for i in range(len(item['company_names'])):
        #         new_item = BursaScrapyItem

        #         new_item['numbers'] = item['numbers'][i]
        #         new_item['announcement_dates'] = item['announcement_dates'][i]
        #         new_item['company_names'] = item['company_names'][i]
        #         new_item['titles'] = item['titles'][i]
        #         new_item['source_urls'] = item['source_urls'][i]

        #         exporter.export_item(new_item)

        #     exporter.finish_exporting()

        
