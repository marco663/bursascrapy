# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy

# def serialize_source_url(url)

class BursaScrapyItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    numbers = scrapy.Field()
    company_names = scrapy.Field()
    announcement_dates = scrapy.Field()
    titles = scrapy.Field()
    descriptions = scrapy.Field()
    source_urls = scrapy.Field()